This is an OpenPGP proof that connects
[my OpenPGP key](https://keyoxide.org/hkp/B55FA7D46863A03889F1EC2C161A3E90E4BA3B6F)
to this Gitea account. For details check out https://keyoxide.org/guides/openpgp-proofs
